<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Enums\FieldLimit;

class CreateContentTypeFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_type_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ct_field_name')->unique();
            $table->string('machine_name')->unique();
            $table->string('hint')->nullable();
            $table->string('placeholder')->nullable();
            $table->tinyInteger('required')->default(0);
            $table->integer('content_type_id')->index()->unsigned();
            $table->integer('field_type_id')->index()->unsigned();
            $table->integer('author_id')->index()->unsigned();
            $table->tinyInteger('limit')->default(0);
            $table->tinyInteger('is_deleted')->default(0);

            $table->foreign('content_type_id')->references('id')->on('content_types')->onUpdate('cascade')->onDelete('cascade');;
            $table->foreign('field_type_id')->references('id')->on('field_types')->onUpdate('cascade')->onDelete('cascade');;
            $table->foreign('author_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_type_fields');
    }
}
