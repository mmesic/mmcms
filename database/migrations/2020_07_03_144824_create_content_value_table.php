<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentValueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_value', function (Blueprint $table) {
            $table->increments('id');
            $table->string('content_title');
            $table->integer('ct_id')->unsigned();
            $table->integer('author_id')->unsigned();
            // $table->integer('ctf_id')->unsigned();
            // $table->integer('ctfv_id')->unsigned();
            $table->tinyInteger('is_deleted')->default(0);
            $table->foreign('ct_id')->references('id')->on('content_types')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('author_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            // $table->foreign('ctf_id')->references('id')->on('content_type_fields')->onUpdate('cascade')->onDelete('cascade');
            // $table->foreign('ctfv_id')->references('id')->on('content_type_field_values')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::table('content_type_field_values', function($table) {
            $table->foreign('content_id')->references('id')->on('content_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_value');
    }
}
