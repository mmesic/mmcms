<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Enums\FieldLimit;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->index()->unsigned();
            $table->string('meta_key');
            $table->string('meta_value');
            $table->integer('field_type_id')->index()->unsigned();
            $table->enum('limit', [
                FieldLimit::LIMITED,
                FieldLimit::UNLIMITED,
            ]);
            $table->tinyInteger('is_deleted')->default(0);
            $table->timestamps();
        });
        Schema::table('profiles', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
