<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFieldTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('field_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('field_name')->unique();
            $table->string('field_db_row');
            $table->string('field_type');
            $table->string('field_input_type')->nullable();
            $table->tinyInteger('is_deleted')->default(0);
            $table->timestamps();
        });
        Schema::table('profiles', function($table) {
            $table->foreign('field_type_id')->references('id')->on('field_types');
        });
      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('field_types');
    }
}
