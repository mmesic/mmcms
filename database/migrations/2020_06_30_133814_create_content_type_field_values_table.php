<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentTypeFieldValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_type_field_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('content_id')->index()->unsigned();
            $table->integer('field_type_id')->index()->unsigned();
            $table->integer('ct_field_id')->index()->unsigned();
            $table->string('string_field_value')->nullable();
            $table->mediumText('medium_text_field_value')->nullable();
            $table->longText('long_text_field_value')->nullable();
            $table->integer('integer_field_value')->nullable();
            $table->mediumInteger('medium_integer_field_value')->nullable();
            $table->smallInteger('small_integer_field_value')->nullable();
            $table->tinyInteger('tiny_integer_field_value')->nullable();
            $table->time('time_field_value')->nullable();
            $table->timestamp('timestamp_field_value')->nullable();
            $table->date('date_field_value')->nullable();
            $table->dateTime('date_time_field_value')->nullable();
            $table->float('float_field_value')->nullable();
            $table->tinyInteger('is_deleted')->default(0);
            $table->timestamps();
        });
        Schema::table('content_type_field_values', function($table) {
            $table->foreign('field_type_id')->references('id')->on('field_types');
            $table->foreign('ct_field_id')->references('id')->on('content_type_fields');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_type_field_values');
    }
}
