<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class FieldTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // INPUT FIELDS
        DB::table('field_types')->insertGetId([
            'field_name' => "Text Input",
            'field_db_row' => "string_field_value",
            'field_type' => "input",
            'field_input_type' => "text",
        ]);

        DB::table('field_types')->insertGetId([
            'field_name' => "Tel Input",
            'field_db_row' => "string_field_value",
            'field_type' => "input",
            'field_input_type' => "tel",
        ]);

        DB::table('field_types')->insertGetId([
            'field_name' => "Number Input",
            'field_db_row' => "integer_field_value",
            'field_type' => "input",
            'field_input_type' => "number",
        ]);

        DB::table('field_types')->insertGetId([
            'field_name' => "Email Input",
            'field_db_row' => "string_field_value",
            'field_type' => "input",
            'field_input_type' => "email",
        ]);

        // CHECKBOX
        DB::table('field_types')->insertGetId([
            'field_name' => "Checkbox",
            'field_db_row' => "tiny_integer_field_value",
            'field_type' => "checkbox",
        ]);

        // checklist
        DB::table('field_types')->insertGetId([
            'field_name' => "Checklist",
            'field_db_row' => "long_text_field_value",
            'field_type' => "checklist",
        ]);

        // radios
        DB::table('field_types')->insertGetId([
            'field_name' => "radios",
            'field_db_row' => "string_field_value",
            'field_type' => "radios",
        ]);

        // select
        DB::table('field_types')->insertGetId([
            'field_name' => "Select",
            'field_db_row' => "string_field_value",
            'field_type' => "select",
        ]);

        // textArea
        DB::table('field_types')->insertGetId([
            'field_name' => "textArea",
            'field_db_row' => "long_text_field_value",
            'field_type' => "textArea",
        ]);

        // DB::table('field_types')->insertGetId([
        //     'field_name' => "Text Medium",
        //     'field_type' => "medium_text_field_value"
        // ]);

        // DB::table('field_types')->insertGetId([
        //     'field_name' => "Text Long",
        //     'field_type' => "long_text_field_value"
        // ]);

        // DB::table('field_types')->insertGetId([
        //     'field_name' => "Integer",
        //     'field_type' => "integer_field_value"
        // ]);

        // DB::table('field_types')->insertGetId([
        //     'field_name' => "Integer Medium",
        //     'field_type' => "medium_integer_field_value"
        // ]);

        // DB::table('field_types')->insertGetId([
        //     'field_name' => "Integer Small",
        //     'field_type' => "small_integer_field_value"
        // ]);

        // DB::table('field_types')->insertGetId([
        //     'field_name' => "Integer Tiny",
        //     'field_type' => "tiny_integer_field_value"
        // ]);

        // DB::table('field_types')->insertGetId([
        //     'field_name' => "Float",
        //     'field_type' => "float_field_value"
        // ]);

        // DB::table('field_types')->insertGetId([
        //     'field_name' => "Time",
        //     'field_type' => "time_field_value"
        // ]);

        // DB::table('field_types')->insertGetId([
        //     'field_name' => "Timestamp",
        //     'field_type' => "timestamp_field_value"
        // ]);
        
        // DB::table('field_types')->insertGetId([
        //     'field_name' => "Date",
        //     'field_type' => "date_field_value"
        // ]);

        // DB::table('field_types')->insertGetId([
        //     'field_name' => "Datetime",
        //     'field_type' => "date_time_field_value"
        // ]);
    }
}
