<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleAdminId = DB::table('roles')->insertGetId([
            'name' => "Admin",
        ]);

        $roleUserId = DB::table('roles')->insertGetId([
            'name' => "User",
        ]);

        $permissionAdminId = DB::table('permissions')->insertGetId([
            'name' => "Admin Content Access",
            'pattern' => "All",
            'target' => "All",
            'module' => "All",
        ]);

        $permissionRoleRelationId = DB::table('permission_role_relation')->insertGetId([
            'permission_id' => $roleAdminId,
            'role_id' => $permissionAdminId,
        ]);

        $userAdminId = DB::table('users')->insertGetId([
            'email' => 'admin@admin.com',
            'password' => Hash::make('admin'),
        ]);

        DB::table('user_role_relation')->insert([
            'role_id' => $roleAdminId,
            'user_id' => $userAdminId,
        ]);

        $userUserId = DB::table('users')->insertGetId([
            'email' => 'user@user.com',
            'password' => Hash::make('user'),
        ]);

        DB::table('user_role_relation')->insert([
            'role_id' => $roleUserId,
            'user_id' => $userUserId,
        ]);
    }
}
