import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import { EventBus } from './../app';
// import GeneralFunctions from './../functions';
// Vue.use(GeneralFunctions);
Vue.use(VueAxios, axios);
export default {
    install(Vue, options){
        let signedIn = false;
        if(typeof localStorage.getItem('access_token') !== "undefined") {
            const token_expires = parseInt(localStorage.getItem('expires_at'));
            const currentTime = new Date().getTime();
            if(currentTime < token_expires){
                signedIn = true;
            }          
        }
        Vue.prototype.signedIn = signedIn;

        Vue.prototype.login = (email, password) => {
            axios.post('/api/token', {
                email: email,
                password: password
            }).then(response => {
              console.log(response);
              localStorage.setItem('access_token', response.data.access_token);
              localStorage.setItem('refresh_token', response.data.refresh_token);
              localStorage.setItem('expires_at', this.convert_seconds_to_timestamp(response.data.expires_in));
              Vue.prototype.signedIn = true;
              this.activateAdminMenu();
            }).catch(error => {
               console.log(error);
               localStorage.removeItem('access_token');
               localStorage.removeItem('refresh_token');
               localStorage.removeItem('expires_at');
               Vue.prototype.signedIn = false;
            });
           
        }
        Vue.prototype.logout = () => {
            localStorage.removeItem('access_token');
            localStorage.removeItem('refresh_token');
            localStorage.removeItem('expires_at');
        }

        Vue.prototype.getCurrentUserRole = () => {
            // console.log(localStorage.getItem('access_token'));
            this.get_current_user_role();
        }

    },
    convert_seconds_to_timestamp(sec){
        var today = new Date();
        var expires_timestamp = today.setSeconds(today.getSeconds() + sec);
        return expires_timestamp;
      },
    activateAdminMenu() {
      EventBus.$emit('activateAdminMenu', true);
    },
    get_current_user_role() {
        let role = null;
        const token = localStorage.getItem('access_token');
        axios.get('/api/auth/user', {
          headers: {
            'Authorization': `Bearer ${token}`
          }
        }).then(response => {
            console.log(response.data.name);
            return response.data.name;
       
      //   localStorage.setItem('access_token', response.data.access_token);
      //   localStorage.setItem('refresh_token', response.data.refresh_token);
      //   localStorage.setItem('expires_at', this.convert_seconds_to_timestamp(response.data.expires_in));
      //   Vue.prototype.signedIn = true;
      //   this.activateAdminMenu();
      }).catch(error => {
         console.log(error);
      //    localStorage.removeItem('access_token');
      //    localStorage.removeItem('refresh_token');
      //    localStorage.removeItem('expires_at');
      //    Vue.prototype.signedIn = false;
      });
    }
}