export default {
  props: ['color', 'mode', 'snackbar', 'text', 'timeout', 'x', 'y'],
  data () {
    return {
      snack_color: '',
      snack_mode: '',
      snack_snackbar: false,
      snack_text: '',
      snack_timeout: 6000,
      snack_x: null,
      snack_y: 'top',
    }
  },
  watch: {
    color(val){
      this.snack_color = val;
    },
    mode(val){
      this.snack_mode = val;
    },
    snackbar(val){
      this.snack_snackbar = val;
    },
    text(val){
      this.snack_text = val;
    },
    timeout(val){
      this.snack_timeout = val;
    },
    x(val){
      this.snack_x = val;
    },
    y(val){
      this.snack_y = val;
    }
  },
  created() {
      this.snack_color = this.color;
      this.snack_mode = this.mode;
      this.snack_snackbar = this.snackbar;
      this.snack_text = this.text;
      this.snack_timeout = this.timeout;
      this.snack_x = this.x;
      this.snack_y = this.y;
  },
}


