
import Vue from 'vue';
import axios from 'axios';
import { EventBus } from '../../../../../app'
import VueAxios from 'vue-axios';
import GeneralFunctions from './../../../../../_helpers/functions';
import Authorization from './../../../../../authorization/authorize';
Vue.use(GeneralFunctions, Authorization);
Vue.use(VueAxios, axios);
export default {
  name: 'resources-js-components-newsletter',
  components: {},
  props: [],
  data () {
    return {
      email: '',
      emailRules: [
        v => !!v || 'E-mail is required',
        v => /.+@.+\..+/.test(v) || 'E-mail must be valid',
      ],
      valid: true,
    }
  },
  computed: {

  },
  mounted () {

  },
  methods: {
    submit(){
      console.log(this.validate());
      if(this.validate()){
        axios.post('/api/user/create', {
          email: this.email,
        }).then(response => {
          let tmp_pw = response.data;
          console.log(response);
          setTimeout(() => {
            this.login(this.email, tmp_pw);
                console.log("logged in");
                this.updateNavigation();
                setTimeout(() => {
                  this.$router.push('/ebook')
                }, 200);
          }, 300);
          
        
          //   this.authorized = true;
        }).catch(error => {
          console.log(error);
        
          console.log(error.response)
        });
      }
    },
   
    validate () {
      return this.$refs.form.validate();
    },
    updateNavigation() {
      EventBus.$emit('loginUser', 'login');
    }
  },
  
}


