
export default {
  name: 'resources-js-components-admin-side-navigation',
  components: {},
  props: [],
  data () {
    return {
      items: [
        { title: 'Dashboard', icon: 'mdi-view-dashboard', url: '/admin/dashboard' },
        { title: 'Users', icon: 'mdi-account-multiple' , url: '/admin/users' },
        { title: 'Content', icon: 'mdi-content-copy' , url: '/admin/content' }, 
        { title: 'Content Types', icon: 'mdi-content-copy' , url: '/admin/content-types' }, 
      ],
      right: null,
    }
  },
  computed: {

  },
  mounted () {

  },
  methods: {

  }
}


