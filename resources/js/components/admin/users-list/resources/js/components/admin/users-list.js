import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
Vue.use(VueAxios, axios);
var Promise = require('promise');
export default  {
  name: 'resources-js-components-admin-users',
  inject: ['accessToken'],
  components: {},
  props: [],
  data () {
    return {
      search: '',
      users: [],
      headers: [
        { text: 'ID', value: 'id',},
        { text: 'Email', value: 'email' },
        { text: 'Verified', value: 'email_verified_at' },
        { text: 'Role', value: 'rorole_idle' },
        { text: 'Created', value: 'created_at' },
        { text: 'Updated', value: 'updated_at' },
      ]
    }
  },
  computed: {

  },
  mounted () {
    this.getUsers();
  },
  methods: {
    getUsers(){
      console.log(localStorage.getItem('access_token'));
      const token = localStorage.getItem('access_token');
        axios.get('/api/users', {
          headers: {
            'Authorization': `Bearer ${token}`
          }
        }).then(response => {
          this.users = response.data;
          // console.log(response);
        }).catch(error => {
          console.log(error)
          // if (error.response.data.error.statusCode === 401) {
          //   console.log("401");
          // }
          // return Promise.reject(error)
                  // console.log(error);
        });
    }
  }
}


