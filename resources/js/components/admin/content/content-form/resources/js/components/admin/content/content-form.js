import { validationMixin } from 'vuelidate'
import { required, maxLength, email } from 'vuelidate/lib/validators'
import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import { EventBus } from '../../../../../../../../../app';
import contentFormSchema from '../../../../../../../../../forms/contentFormSchema';
import VueFormGenerator from 'vue-form-generator';
import 'vue-form-generator/dist/vfg-core.css';
Vue.use(VueFormGenerator)

window.VueFormGenerator = VueFormGenerator
Vue.use(VueAxios, axios);
export default {
  mixins: [validationMixin],

  validations: {
    name: { required, maxLength: maxLength(10) },
    email: { required, email },
    select: { required },
    checkbox: {
      checked (val) {
        return val
      },
    },
    fields: [],
  },

  data: () => ({
    content_type: '',
    model: {},
    schema: {},
    formOptions: {
      validateAfterLoad: false,
      validateAfterChanged: true,
      validateAsync:true
    },
    submitField: {},
    titleField: {
      type: "input",
      inputType: "text",
      label: "Title",
      model: "content_title",
      maxlength: 50,
      required: true,
      placeholder: "Content title",
      validator: ['string']
    },
    hiddenField: {
      type: "input",
      inputType: "hidden",
      model: "content_type",
    },
    isSaving: false,
    isEdit: false,
  }),
  created() {
    this.content_type = this.$route.params.content_type;
    this.getContentTypeFields();
    if(this.$route.params.id){
      this.setupUpdateBtn();
      this.isEdit = true;
      this.loadFieldValues(this.$route.params.id);
    } else {
      this.setupCreateBtn();
    }
  },
  computed: {
  },
  methods: {
    submit(model) {
      console.log(model);
    },
    clear () {
      this.$v.$reset()
      this.name = ''
      this.email = ''
      this.select = null
      this.checkbox = false
    },
    getContentTypeFields(){
      const token = localStorage.getItem('access_token');
      axios.get('/api/auth/content/content-type-fields/'+this.content_type, {
        headers: {
          'Authorization': `Bearer `+localStorage.getItem('access_token')
        }
      }).then(response => {
        console.log(response);
        this.fields = response.data;
        this.schema = Object.assign({}, response.data.schema);
        this.schema.fields.splice(0, 0, this.titleField);
        this.schema.fields[this.schema.fields.length] = this.submitField;
        
        this.model.content_type = this.content_type;
        this.model.content_title = '';
      }).catch(error => {
        console.log(error);
      });
    },
    loadFieldValues(id){
        axios.get('/api/auth/content/content-values/'+id, {
          headers: {
            'Authorization': `Bearer `+localStorage.getItem('access_token')
          }
        }).then(response => {
          console.log(response);
          this.model = response.data;
          this.model.id = id;
          this.model.content_type = this.content_type;
        }).catch(error => {
          console.log(error);
        });
    },
    setupCreateBtn(){
      this.submitField = {
        type:'submit',
        onSubmit(model){
          const token = localStorage.getItem('access_token');
          axios.post('/api/auth/content/save', {
            model: model,
            content_type: model.content_type,
            content_title: model.content_title
            }, {
            headers: {
              'Authorization': `Bearer ${token}`
            }
          }).then(response => {
            console.log(response);
          }).catch(error => {
            console.log(error);
          });
        },
        label: '',
        buttonText: 'Save',
        validateBeforeSubmit: true
      }

    },
    setupUpdateBtn(){
      this.submitField = {
        type:'submit',
        onSubmit(model){
          const token = localStorage.getItem('access_token');
          axios.post('/api/auth/content/update/'+model.id, {
            model: model,
            content_type: model.content_type,
            content_title: model.content_title,
            id: model.id
            }, {
            headers: {
              'Authorization': `Bearer ${token}`
            }
          }).then(response => {
            console.log(response);
          }).catch(error => {
            console.log(error);
          });
        },
        label: '',
        buttonText: 'Update',
        validateBeforeSubmit: true
      }
    }

  },
}

