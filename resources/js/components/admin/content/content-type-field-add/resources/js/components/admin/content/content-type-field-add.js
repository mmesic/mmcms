import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import { EventBus } from '../../../../../../../../../app';
Vue.use(VueAxios, axios);

export default {
  name: 'resources-js-components-admin-content-content-type-field-add',
  components: {},
  props: [],
  data () {
    return {
      ct_id: null,
      dialog: false,
      valid: true,
      ct_field_name: '',
      machine_name: "field_",
      ctFieldRules: [
        v => !!v || 'Title is required',
        v => this.fieldNameUnique || 'Field name has to bee unique'
      ],
      fieldNameUnique: true,
      awaitingSearch: false,
      fieldType: null,
      fields: [],
      selectedLimit: 0,
      limit: [
        { text: 'Limited', value: 0 },
        { text: 'Unlimited', value: 1 }
      ],
      fieldsList: [],
      snackbar: false,
      snackbarColor: '',
      snackbarText: '',
      snackbarComponentKey: 0,
      field_hint: '',
      field_placeholder: '',
      field_required: 0,

    }
  },
  computed: {

  },
  created() {
    this.ct_id = this.$route.params.id;
  },
  mounted () {
    this.getFieldTypes();
  },
  watch: {
    ct_field_name() {
      if (!this.awaitingSearch) {
        setTimeout(() => {
          // this.checkIsTitleUnique(this.name);
          this.awaitingSearch = false;
        }, 1000); // 1 sec delay
      }
      this.awaitingSearch = true;
      this.machine_name = "field_"+this.ct_field_name.toLowerCase().replace(/ /g,"_");
    }
  },
  methods: {
    validate () {
      return this.$refs.form.validate();
    },

    checkIsFieldNameUnique(value){
      const token = localStorage.getItem('access_token');
      axios.post('/api/auth/content_type/validate', {
        name: value
        }, {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      }).then(response => {
        console.log(response);
        console.log(Object.keys(response.data).length !== 0);
        if(Object.keys(response.data).length !== 0){
          this.fieldNameUnique = false;
        }else {
          this.fieldNameUnique = true;
        }
        
      }).catch(error => {
        console.log(error);
      });
    },
    getFieldTypes(){
      const token = localStorage.getItem('access_token');
      axios.get('/api/auth/content/field-types', {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      }).then(response => {
        console.log(response);
        this.fields = response.data;
      }).catch(error => {
        console.log(error);
      });
    },
    createNewField(){
      console.log(this.selectedLimit);
      if(this.validate()){
        const token = localStorage.getItem('access_token');
        axios.post('/api/auth/content-type-field/create', {
          ct_field_name: this.ct_field_name,
          machine_name: this.machine_name,
          field_type_id: this.fieldType,
          content_type_id: this.ct_id,
          limit: this.selectedLimit,
          hint: this.field_hint,
          placeholder: this.field_placeholder,
          required: this.field_required
          }, {
          headers: {
            'Authorization': `Bearer ${token}`
          }
        }).then(response => {
          this.snackbarComponentKey += 1;
          this.dialog = false;
          this.snackbar = true;
          this.snackbarColor = 'success'
          this.snackbarText = 'Field Added Succesfully';
          EventBus.$emit('refreshFieldList', true);
          this.clearFormfields();
        }).catch(error => {
          this.snackbarComponentKey += 1;
          this.dialog = false;
          console.log(error);
          this.snackbar = true;
          this.snackbarColor = 'error'
          this.snackbarText = 'Error';
          this.clearFormfields();
        });
      }
    },
    clearFormfields(){
      this.ct_field_name = '';
      this.machine_name = 'field_';
      this.fieldType = null;
      this.selectedLimit = null;
      this.field_hint = '';
      this.field_placeholder = '';
      this.field_required = 0;
    }

  }
}





