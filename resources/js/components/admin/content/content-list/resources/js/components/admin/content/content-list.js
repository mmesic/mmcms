import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
// import { EventBus } from '../../../../../../../../../app';
Vue.use(VueAxios, axios);
export default {
  name: 'resources-js-components-admin-content-content-list',
  components: {},
  props: [],
  data () {
    return {
      ct_id: null,
      search: '',
      headers: [
        { text: 'Title', align: 'start', value: 'content_title' },
        { text: 'Content Type', value: 'ct_name'},
        { text: 'Author', value: 'author'},
        { text: 'Created', value: 'created_at' },
        { text: 'Updated', value: 'updated_at' },
        { text: 'Action', value: 'id' },
      ],
      contentList: [],
    }
  },
  computed: {

  },
  mounted () {
    this.getContentList();
  },
  methods: {
    getContentList(){
      const token = localStorage.getItem('access_token');
      axios.get('/api/auth/content/list', {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      }).then(response => {
        console.log(response);
        this.contentList = response.data;
      }).catch(error => {
        console.log(error);
      });
    }
  }
}


