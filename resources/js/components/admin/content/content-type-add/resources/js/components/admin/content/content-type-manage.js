import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import { EventBus } from '../../../../../../../../../app'
Vue.use(VueAxios, axios);

export default {
  name: 'resources-js-components-admin-content-content-type-manage',
  components: {},
  props: [],
  data () {
    return {
      dialog: false,
      valid: true,
      name: '',
      description: '',
      ctRules: [
        v => !!v || 'Title is required',
        v => this.titleUnique || 'Title has to bee unique'
      ],
      titleUnique: true,
      awaitingSearch: false,
      snackbar: false,
      snackbarColor: '',
      snackbarText: '',
      snackbarComponentKey: 0,
    }
  },
  computed: {

  },
  mounted () {

  },
  watch: {
    name() {
      if (!this.awaitingSearch) {
        setTimeout(() => {
          this.checkIsTitleUnique(this.name);
          this.awaitingSearch = false;
        }, 1000); // 1 sec delay
      }
      this.awaitingSearch = true;
    
    }
  },
  methods: {
    submitForm() {
      if(this.validate()){
        console.log(this.name);
        console.log(this.description);
        this.createContentType(this.name, this.description);
      } else {
        console.log("invalid form");
      }
    },
    validate () {
      return this.$refs.form.validate();
    },
    createContentType(name, description){
      const token = localStorage.getItem('access_token');
      axios.post('/api/auth/content_type/create', {
        ct_name: name,
        ct_description: description
        }, {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      }).then(response => {
        this.snackbarComponentKey += 1;
        this.dialog = false;
        this.snackbar = true;
        this.snackbarColor = 'success'
        this.snackbarText = 'Content Type Added Succesfully';
        EventBus.$emit('refreshContentTypeList', true);
        this.clearFormfields();
        // console.log(response);
      }).catch(error => {
        this.snackbarComponentKey += 1;
        this.dialog = false;
        this.snackbar = true;
        this.snackbarColor = 'error'
        this.snackbarText = 'Error';
        this.clearFormfields();
        // console.log(error);
      });
    },
    checkIsTitleUnique(value){
      const token = localStorage.getItem('access_token');
      axios.post('/api/auth/content_type/validate', {
        name: value
        }, {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      }).then(response => {
        if(Object.keys(response.data).length !== 0){
          this.titleUnique = false;
        }else {
          this.titleUnique = true;
        }
        
      }).catch(error => {
        console.log(error);
      });
    },
    clearFormfields(){
      this.name = '';
      this.description = '';
    }

  }
}


