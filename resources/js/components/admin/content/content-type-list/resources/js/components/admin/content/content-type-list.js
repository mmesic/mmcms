import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import { EventBus } from '../../../../../../../../../app';
Vue.use(VueAxios, axios);

export default {
  data () {
    return {
      search: '',
      headers: [
        { text: 'Title', align: 'start', value: 'ct_name' },
        { text: 'Description', value: 'ct_description' },
        { text: 'Created', value: 'created_at' },
        { text: 'Updated', value: 'updated_at' },
        { text: 'Action', value: 'id' },
      ],
      ct_list: [],
    }
  },
  mounted() {
    this.getContentTypeList();
  },
  created (){
    EventBus.$on('refreshContentTypeList', (data) => {
      if(data){
        this.getContentTypeList();
      }
    })
  },
  methods: {
    getContentTypeList(){
      const token = localStorage.getItem('access_token');
      axios.get('/api/auth/content_type/list', {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      }).then(response => {
        console.log(response);
        this.ct_list = response.data
      }).catch(error => {
        console.log(error);
      });
    }
  },
}


