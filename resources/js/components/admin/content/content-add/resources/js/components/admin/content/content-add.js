import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
// import { EventBus } from '../../../../../../../../../app';
Vue.use(VueAxios, axios);
export default {
  name: 'resources-js-components-admin-content-content-add',
  components: {},
  props: [],
  data () {
    return {
      dialog: false,
      contentTypes: [],
    }
  },
  computed: {

  },
  mounted () {
    this.getContentTypeList();
  },
  methods: {
    getContentTypeList(){
      const token = localStorage.getItem('access_token');
      axios.get('/api/auth/content_type/list', {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      }).then(response => {
        console.log(response);
        this.contentTypes = response.data
      }).catch(error => {
        console.log(error);
      });
    },
    openForm(machine_name){
      this.$router.push('/admin/content/'+machine_name);
    }
  }
}


