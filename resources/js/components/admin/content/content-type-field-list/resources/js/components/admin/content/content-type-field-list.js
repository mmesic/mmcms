import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import { EventBus } from '../../../../../../../../../app';
Vue.use(VueAxios, axios);

export default {
  name: 'resources-js-components-admin-content-content-type-field-list',
  components: {},
  props: [],
  data () {
    return {
      ct_id: null,
      search: '',
      headers: [
        { text: 'Title', align: 'start', value: 'ct_name' },
        { text: 'Machine name', value: 'machine_name'},
        { text: 'Limit', value: 'limit' },
        { text: 'Created', value: 'created_at' },
        { text: 'Updated', value: 'updated_at' },
        { text: 'Action', value: 'id' },
      ],
      fieldsList: [],
    }
  },
  created (){
    this.ct_id = this.$route.params.id;
    EventBus.$on('refreshFieldList', (data) => {
      if(data){
        this.getContentTypeFields();
      }
    })
  },
  mounted() {
    this.getContentTypeFields();
  },
  methods: {
    getContentTypeFields(){
      const token = localStorage.getItem('access_token');
      axios.get('/api/auth/content/field-type-fields/'+this.ct_id, {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      }).then(response => {
        console.log(response);
        this.fieldsList = response.data;
      }).catch(error => {
        console.log(error);
      });
    },
  },
}


