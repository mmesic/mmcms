// src/forms/userFormSchema.js

import VueFormGenerator from 'vue-form-generator'

export default {
  groups: [
    {
        legend: 'Personal Info',
        fields: [
          {
            type: 'input',
            inputType: 'text',
            label: 'First Name',
            model: 'first_name',
            required: true,
            validator: ['string', 'required']
          },
          {
            type: 'input',
            inputType: 'text',
            label: 'Last Name',
            model: 'last_name',
            required: true,
            validator: ['string', 'required']
          },
          {
            type: 'select',
            label: 'Gender',
            model: 'gender',
            values: [
              {id: 'male', name: 'Male'},
              {id: 'female', name: 'Female'}
            ],
            selectOptions: {
              noneSelectedText: 'Choose One'
            },
            required: true,
            validator: ['string', 'required']
          },
          {
            type: 'input',
            inputType: 'number',
            label: 'Age',
            model: 'age',
            required: true,
            hint: 'Age is required & must be a between 18 and 35.',
            validator: ['number', 'required'],
            min: 18,
            max: 35
          },
          {
            type: 'input',
            inputType: 'text',
            label: 'City',
            model: 'city',
            required: true,
            validator: ['string', 'required']
          }
        ]
      },
  ],
  created() {
      
  },
  methods: {
      
  },
}