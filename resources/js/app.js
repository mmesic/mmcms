import Vue from 'vue';
import { router } from "./_helpers/router";
import axios from 'axios'
import VueAxios from 'vue-axios'
import './_helpers/component.js';
import '@mdi/font/css/materialdesignicons.css' ;
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import Authorization from './authorization/authorize';
import VueFormGenerator from 'vue-form-generator'

Vue.use(Vuetify, VueAxios, axios, Authorization, VueFormGenerator);
export const EventBus = new Vue();
new Vue({
    el:"#app",
    router,
    vuetify : new Vuetify(),
});

