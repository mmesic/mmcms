export default {
      install(Vue, options){
        let isMobile = false;
        if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            isMobile = true;
          }
        Vue.prototype.isMobile = isMobile;
         
        Vue.prototype.convert_seconds_to_timestamp = (sec) => {
          var today = new Date();
          var expires_timestamp = today.setSeconds(today.getSeconds() + sec);
          return expires_timestamp;
        }
    }
}