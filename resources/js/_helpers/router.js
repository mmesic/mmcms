import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import VueRouter from 'vue-router';
import Authorization from './../authorization/authorize';
import GeneralFunctions from './../_helpers/functions';
import { Role } from './role';

import HomepageComponent from './../components/homepage/HomepageComponent'
import Logout from './../components/Logout';
import PassportComponent from './../components/passport/PassportComponent';
import eBookComponent from './../components/pages/ebook/eBookComponent';
import AdminDashboardComponent from './../components/admin/dashboard/DashboardComponent';
import AdminUsersComponent from './../components/admin/users/UsersComponent';
import ContentTypesComponent from './../components/admin/content/content-types/ContentTypesComponent';
import ContentTypeEdit from './../components/admin/content/content-type-edit/ContentTypeEdit';
import PageNotFound from './../components/page-not-found/PageNotFoundComponent';
import ContentComponent from './../components/admin/content/content/ContentComponent';
import contentFormPage from './../components/admin/content/content-form-page/contentFormPage';
import { EventBus } from '../app';

Vue.use(VueRouter);
Vue.use(GeneralFunctions);
Vue.use(Authorization);
Vue.use(VueAxios, axios);
export const router = new VueRouter({
    routes: [
        { 
            path: '/' ,
            component: HomepageComponent
        },
        { 
            path: '/ebook' ,
            component: eBookComponent 
        },
        {
            path: '/admin/dashboard',
            component: AdminDashboardComponent,
            meta: { authorizedRoute: [Role.Admin] } 
        },
        {
            path: '/admin/users',
            component: AdminUsersComponent,
            meta: { authorizedRoute: [Role.Admin] } 
        },
        {
            path: '/admin/content',
            component: ContentComponent,
            meta: { authorizedRoute: [Role.Admin] } 
        },
        {
            path: '/admin/content/:content_type',
            component: contentFormPage,
            meta: { authorizedRoute: [Role.Admin] } 
        },
        {
            path: '/admin/content/:content_type/:id/edit',
            component: contentFormPage,
            meta: { authorizedRoute: [Role.Admin] } 
        },
        {
            path: '/admin/content-types',
            component: ContentTypesComponent,
            meta: { authorizedRoute: [Role.Admin] } 
        },
        {
            path: '/admin/content-types/:id',
            component: ContentTypeEdit,
            meta: { authorizedRoute: [Role.Admin] } 
        },
        {
            path: '*', component: PageNotFound
        },
    ],
    mode: 'history'
});
router.beforeEach((to, from, next) => {
    // console.log(Authorization);
    // redirect to login page if not logged in and trying to access a restricted page
    // console.log("to "+to.path);
    // console.log("from " +from.path);
    const { authorizedRoute } = to.meta;
   
    if (authorizedRoute) {
        let role = null;
        const token = localStorage.getItem('access_token');
        axios.get('/api/auth/user', {
          headers: {
            'Authorization': `Bearer ${token}`
          }
        }).then(response => {
            // console.log(response.data.name);
            const userRole = response.data.name;
            if(authorizedRoute[0] === userRole || userRole === "Admin"){
                if(userRole === "Admin"){
                    EventBus.$emit('isAdmin', true);
                }
                if(to.path !== to.path){
                    return next({ path: to.path });
                }
            } else {
                if(to.path !== "/404"){
                    return next({ path: "/404" });
                }
                EventBus.$emit('isAdmin', false);
            } 
           
      }).catch(error => {
         console.log(error.response.status);
        //  Authorization.logout();
        
         EventBus.$emit('isAnonymous', true);
         EventBus.$emit('isAdmin', false);
         if(to.path !== "/404"){
            return next({ path: "/404" });
        }
      });
    }
    EventBus.$emit('isAdmin', false);
    next();
});