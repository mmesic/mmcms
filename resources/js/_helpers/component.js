
import Vue from 'vue';

import App from './../components/App';
Vue.component('App', App);

import LoginComponent from './../components/LoginComponent';
Vue.component('login', LoginComponent);

import FooterComponent from './../components/footer/FooterComponent';
Vue.component('footer-component', FooterComponent);

import NavigationComponent from './../components/navigation/NavigationComponent';
Vue.component('top-nav-component', NavigationComponent);

import HomepageComponent from './../components/homepage/HomepageComponent';
Vue.component('homepage', HomepageComponent);

import UserRegistration from './../components/user-registration/UserRegistration';
Vue.component('user-registration', UserRegistration);

import GridContentListComponent from './../components/content-lists/grid-content-list/GridContentListComponent';
Vue.component('grid-content-list-component', GridContentListComponent);

import NewsletterComponent from './../components/newsletter/index';
Vue.component('newsletter-component', NewsletterComponent);

import UserTopNavigation from './../components/user/top-navigation/TopNavigation';
Vue.component('user-top-navigation', UserTopNavigation);

/**************** ADMIN COMPONENTS ******************/

import AdminTopNavigation from './../components/admin/top-navigation/TopNavigation';
Vue.component('admin-top-navigation', AdminTopNavigation);

import AdminSideNavigation from './../components/admin/side-navigation/SideNavigation';
Vue.component('admin-side-navigation', AdminSideNavigation);

import UsersList from './../components/admin/users-list/UsersListComponent';
Vue.component('users-list', UsersList);

import ContentTypeList from './../components/admin/content/content-type-list/ContentTypeList';
Vue.component('content-type-list', ContentTypeList);

import ContentTypeAdd from './../components/admin/content/content-type-add/ContentTypeAdd';
Vue.component('content-type-add', ContentTypeAdd);

import ContentTypeFieldAdd from './../components/admin/content/content-type-field-add/ContentTypeFieldAdd';
Vue.component('content-type-field-add', ContentTypeFieldAdd);

import ContentTypeFieldEdit from './../components/admin/content/content-type-field-edit/ContentTypeFieldEdit';
Vue.component('content-type-field-edit', ContentTypeFieldEdit);

import ContentTypeFieldList from './../components/admin/content/content-type-field-list/ContentTypeFieldList';
Vue.component('content-type-field-list', ContentTypeFieldList);

import ContentListComponent from './../components/admin/content/content-list/ContentListComponent';
Vue.component('content-list', ContentListComponent);

import ContentAdd from './../components/admin/content/content-add/ContentAdd';
Vue.component('content-add', ContentAdd);

import Snackbar from './../components/snackbar/Snackbar';
Vue.component('snackbar', Snackbar);

import contentFormComponent from './../components/admin/content/content-form/contentFormComponent';
Vue.component('content-form', contentFormComponent);