<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CMS Multisite</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}" >
    </head>
    <body>
        <div id="app">
        <root-component></root-component>
            <!-- @if(\Auth::check())  -->
           
            <!-- @endif -->
        </div>
    <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
