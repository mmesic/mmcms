<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static LIMITED()
 * @method static static UNLIMITED()
 */
final class FieldLimit extends Enum
{
    const LIMITED = 0;
    const UNLIMITED = 1;
}
