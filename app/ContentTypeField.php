<?php

namespace App;
use FieldType;
use Illuminate\Database\Eloquent\Model;

class ContentTypeField extends Model
{
    protected $table = 'content_type_fields';
    
    protected $fillable = [
        'ct_field_name', 'machine_name', 'content_type_id', 'field_type_id', 'author_id' , 'limit', 'hint', 'placeholder', 'required'
    ];

    public function field_types()
    {
        return $this->hasOne('App\FieldType','id','field_type_id');
        // return $this->hasOne(FieldType::class);
    }
}
