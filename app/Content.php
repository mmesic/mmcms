<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = 'content_value';
    
    protected $fillable = [
        'ct_id', 'author_id','is_deleted', 'content_title'
    ];

    public function author()
    {
        return $this->hasOne('App\User','id','author_id');
    }

    public function field_values(){
        return $this->hasMany('App\ContentTypeFieldValue','content_id','id');
    }

    public function content_type(){
        return $this->hasOne('App\ContentType','id','ct_id');
    }

}
