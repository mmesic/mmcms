<?php

namespace App;
use FieldType;
use Illuminate\Database\Eloquent\Model;

class ContentTypeFieldValue extends Model
{
    protected $table = 'content_type_field_values';
    
    protected $fillable = [
        'field_type_id',
        'content_id', 
        'ct_field_id',
        'string_field_value',
        'medium_text_field_value',
        'long_text_field_value',
        'integer_field_value',
        'medium_integer_field_value',
        'small_integer_field_value',
        'tiny_integer_field_value',
        'time_field_value',
        'timestamp_field_value',
        'date_field_value',
        'date_time_field_value',
        'float_field_value',
        'is_deleted',
    ];

}
