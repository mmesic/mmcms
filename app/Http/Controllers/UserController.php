<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Role;
use App\Permission;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function allUsers(){
        return Response::json(User::all());
    }

    public function isUserAuthenticated(Request $request){
        $currentUserId = Auth::id();
        $userRoleRelation = DB::table('user_role_relation')->where('user_id', $currentUserId)->first();
        $role = Role::where('id',  $userRoleRelation->role_id)->take(1)->get();
        return response()->json($role[0]);
    }

    
}
