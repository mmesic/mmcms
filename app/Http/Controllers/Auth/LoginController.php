<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use League\OAuth2\Server\Exception\OAuthServerException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::ROOT;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    // public function logout(){
    //     Auth::logout();
    // }

    public function getToken(Request $request){

   
        $request->request->add([
            'grant_type' => 'password',
            'client_id' => 2,
            'client_secret' => 'iW8IZsLcnKzZOMaGeJ9tkHB1njCVZGd1pFey5KFi',
            'username' => $request->email,
            'password' => $request->password,
            'scope' => '*',
        ]);

        $requestToken = Request::create(env('APP_URL').'/oauth/token', 'post');
        $response = Route::dispatch($requestToken);

        if($response->getStatusCode() == 400 || $response->getStatusCode() == 401){
            return response()->json('Your credentials are incorrect. Please try again'.$response->getStatusCode(), $response->getStatusCode());
        }

        if($response->getStatusCode() !== 200 && $response->getStatusCode() !== 400 && $response->getStatusCode() !== 401){
            return response()->json('Something went wrong on the server'.$response->getStatusCode(),$response->getStatusCode());
        }

        return $response;

    }
    public function refreshToken(Request $request){
        $request->request->add([
            'grant_type' => 'refresh_token',
            'refresh_token' =>  $request->refresh_token,
            'client_id' => 2,
            'client_secret' => 'iW8IZsLcnKzZOMaGeJ9tkHB1njCVZGd1pFey5KFi',
            'scope' => '',
        ]);

    }

    public function logout(Request $request){

    }

}
