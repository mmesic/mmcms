<?php

namespace App\Http\Controllers;

use App\ContentType;
use App\ContentTypeField;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Auth;

class ContentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validation = $request->validate([
            'ct_name' => 'required|unique:content_types'
        ]);

        $ct = ContentType::create([
            'ct_name' => $request->ct_name,
            'machine_name' => preg_replace('/\s+/', '_', strtolower($request->ct_name)),
            'ct_description' => $request->ct_description,
            'author_id' => Auth::id()
        ]);
        return response()->json($ct);
    }

    public function CheckIsTitleUsed(Request $request){
        $ct = ContentType::where('ct_name', $request->name)->first();
        return response()->json($ct);
    }

    public function list(){
        $ct_list = ContentType::all();
        return response()->json($ct_list);
    }

    public function getContentTypeFields($content_type){
        $ct = ContentType::where('machine_name', $content_type)->first();
        $matchValues = ['content_type_id'=> $ct->id, 'is_deleted'=> 0];
        $fields = ContentTypeField::where($matchValues)
                                    ->with(['field_types'])
                                    ->get();
        $response['schema'] = $this->createFormSchema($fields);
        $response['model'] = $this->createFormModel($fields);
        // $fields = DB::table('content_type_fields')
        //               ->where('content_type_fields.content_type_id', $ct->id)
        //               ->leftJoin('field_types', 'content_type_fields.id', '=', 'field_types.id')
        //               ->select('content_type_fields.*' )
        //               ->get();

        // $fields = 
        // $fields = ContentTypeField::leftJoin('field_types', 'users.id', '=', 'posts.user_id')

        return response()->json($response);
    }

    public function createFormSchema($data){
        $schema = [];
        $fields = [];
        foreach($data as $key => $field){
            $temp_field = [];
            $temp_field['type'] = $field->field_types->field_type;
            if(!empty($field->field_types->field_input_type)){
                $temp_field['inputType'] = $field->field_types->field_input_type;
            }
            if(!empty($field->ct_field_name)){
                $temp_field['label'] = $field->ct_field_name;
            } 
            if(!empty($field->machine_name)){
                $temp_field['model'] = $field->machine_name;
            } 
            if(!empty($field->hint)){
                $temp_field['hint'] = $field->hint;
            } 
            if(!empty($field->required)){
                $temp_field['required'] = $field->required;
            } 
            if(!empty($field->placeholder)){
                $temp_field['placeholder'] = $field->placeholder;
            } 
            
            if($field->field_types->field_type == 'input' && $field->field_types->field_input_type == 'text'){
                $temp_field['validator'] = 'string';
            } else if($field->field_types->field_type == 'input' && $field->field_types->field_input_type == 'number'){
                $temp_field['validator'] = 'number';
            } else if($field->field_types->field_type == 'input' && $field->field_types->field_input_type == 'email'){
                $temp_field['validator'] = 'email';
            }
            
            array_push($fields, $temp_field);
        }

        $schema["fields"] = $fields;
        return $schema;
    }

    public function createFormModel($data){
        $model = [];
        // $temp_model = [];
        foreach($data as $key => $field){
            if(!empty($field->machine_name)){
                $model[$field->machine_name] = '';
            }
        }
        // array_push($model, $temp_field);

        return $model;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContentType  $contentType
     * @return \Illuminate\Http\Response
     */
    public function show(ContentType $contentType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContentType  $contentType
     * @return \Illuminate\Http\Response
     */
    public function edit(ContentType $contentType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContentType  $contentType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContentType $contentType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContentType  $contentType
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContentType $contentType)
    {
        //
    }
}
