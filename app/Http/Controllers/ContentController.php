<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContentTypeFieldValue;
use App\ContentType;
use App\Content;
use App\FieldType;
use App\ContentTypeField;
use Auth;
class ContentController extends Controller
{
   public function saveContent(Request $request){
      $validation = $request->validate([
         'content_type' => 'required',
         'content_title' => 'required'
      ]);

      $ct = ContentType::where('machine_name', $request->content_type)->first();

      $content = Content::create([
         'content_title' => $request->content_title,
         'ct_id' => $ct->id, 
         'author_id' => Auth::id(),
      ]);

      $schema = $this->createContentTypeFieldValueSchema($request->model, $content->id); 

      $response = $this->saveContentFieldValues($schema);
  
      return response()->json($schema);
   }
   private function createContentTypeFieldValueSchema($fields, $content_id){

      $schema = [];
      foreach ($fields as $field => $value) {
         if($field !== 'content_type' && $field !== 'content_title'){
            $temp_field = [];
            $ctf = ContentTypeField::where('machine_name', $field)->first();
            $ft = FieldType::where('id', $ctf->field_type_id)->first();
            $temp_field[$ft->field_db_row] = $value;
            $temp_field['content_id'] = $content_id;
            $temp_field['field_type_id'] = $ctf->field_type_id;
            $temp_field['ct_field_id'] = $ctf->id;
            array_push($schema, $temp_field);
         }
      }

      return $schema;
   }

   private function saveContentFieldValues($schema){
      $field_value_ids = [];
      foreach ($schema as $key => $value) {
         $ctfv = ContentTypeFieldValue::create($value);
         array_push($field_value_ids, $ctfv->id);
      }
      return $field_value_ids;
   }

   public function getContents(){
     $contents = Content::where('is_deleted', 0)
                        ->with(['author', 'content_type'])
                        ->get();
     return response()->json($contents);
   }

   public function getContentValues($id){
      $contents = Content::where('id', $id)
                         ->with(['author', 'field_values'])
                         ->first();

      $model = $this->createFormModel($contents);
      return response()->json($model);
   }

   private function createFormModel($contents){
      $model = [];
      $model['content_title'] = $contents->content_title;
      foreach ($contents->field_values as $key => $content) {
         $ctf = ContentTypeField::where('id', $content->ct_field_id)->first();
         $ft = FieldType::where('id', $content->field_type_id)->first();
         $model[$ctf->machine_name] = $content[$ft->field_db_row];
      }
      return $model;
   }

   public function updateContent(Request $request){
      $validation = $request->validate([
         'content_type' => 'required',
         'content_title' => 'required',
         'id' => 'required',
      ]);

      $content = Content::find($request->id);
      $content->update([
         'content_title' => $request->content_title,
      ]);

      $this->updateContentFields($request->model, $request->id);


      return response()->json($content);
   }

   private function updateContentFields($model, $ct_id){
      if(!empty($model)){
         foreach ($model as $field => $value) {
            if($field !== 'content_type' && $field !== 'content_title' && $field !== 'id'){
               $ctf = ContentTypeField::where('machine_name', $field)->first();
               $ft = FieldType::where('id', $ctf->field_type_id)->first();
               $matchThese = ['content_id' => $ct_id, 'field_type_id' => $ft->id, 'ct_field_id' => $ctf->id];
               $ctfv = ContentTypeFieldValue::where($matchThese)->first();
               if(!empty($ctfv)){
                  $ctfv->update([
                     $ft->field_db_row => $value,
                  ]);
               } else {
                  $ctfv = ContentTypeFieldValue::create([
                    'content_id' => $ct_id,
                    'field_type_id' => $ft->id,
                    'ct_field_id' => $ctf->id,
                    $ft->field_db_row => $value,
                  ]);
               }
            }
         }
      }
   }

   public function getContentByContentType($content_type){
      $ct = ContentType::where('machine_name', $content_type)->first();
      $contents = Content::where('ct_id', $ct->id)
                         ->with(['author', 'field_values'])
                         ->get();
      $model = [];
      foreach ($contents as $key => $content) {
         $temp_model = $this->createFormModel($content);
         array_push($model, $temp_model);
      }


      return response()->json($model);
   }
}
