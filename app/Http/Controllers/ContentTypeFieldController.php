<?php

namespace App\Http\Controllers;

use App\ContentTypeField;
use Illuminate\Http\Request;
use Auth;

class ContentTypeFieldController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->validate([
            'ct_field_name' => 'required',
            'machine_name' => 'required',
            'content_type_id' => 'required',
            'field_type_id' => 'required'
        ]);


        $contentTypeField = ContentTypeField::create([
            'ct_field_name' => $request->ct_field_name,
            'machine_name' => $request->machine_name,
            'content_type_id' => $request->content_type_id,
            'field_type_id' => $request->field_type_id,
            'limit' => intval($request->limit),
            'author_id' => Auth::id(),
            'hint'=> $request->hint,
            'placeholder'=> $request->placeholder,
            'required'=> $request->required
        ]);

        return response()->json($contentTypeField);

    }

    public function getFields($id){
        $matchValues = ['content_type_id'=> $id, 'is_deleted'=> 0];
        $ct_fields = ContentTypeField::where($matchValues)->get();
        return response()->json($ct_fields);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContentTypeField  $contentTypeField
     * @return \Illuminate\Http\Response
     */
    public function show(ContentTypeField $contentTypeField)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContentTypeField  $contentTypeField
     * @return \Illuminate\Http\Response
     */
    public function edit(ContentTypeField $contentTypeField)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContentTypeField  $contentTypeField
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContentTypeField $contentTypeField)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContentTypeField  $contentTypeField
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContentTypeField $contentTypeField)
    {
        //
    }
}
