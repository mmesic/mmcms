<?php

namespace App\Http\Controllers;

use App\FieldType;
use Illuminate\Http\Request;
use Auth;

class FieldTypeController extends Controller
{
    public function getFieldTypes()
    {
        $fieldTypes = FieldType::all();
        return response()->json($fieldTypes);
    }
}
