<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentType extends Model
{
      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'content_types';
    
    protected $fillable = [
        'ct_name', 'machine_name', 'ct_description', 'author_id'
    ];

  
}
