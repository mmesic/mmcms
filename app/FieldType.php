<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FieldType extends Model
{
    protected $table = 'field_types';
    
    protected $fillable = ['field_name', 'field_type'];
}
