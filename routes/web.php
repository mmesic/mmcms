<?php

use Illuminate\Support\Facades\Route;

Auth::routes();


// Rewrite default login, register & password routes
Route::get('/login', function () {
    return redirect('/');
});
Route::get('/register', function () {
    return redirect('/');
});
Route::get('/password/{any}', function () {
    return redirect('/');
});

Route::get('/{any}', 'AppController@index')->where('any', '^((?!api).)*')->name('index');
;