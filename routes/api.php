<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/token', 'Auth\LoginController@getToken');

// Route::middleware('auth:api')->group( function () {

//     // Route::get('/user', 'UserController@index');
   
//     Route::get('authenticated', 'UserController@isUserAuthenticated');
    
//     Route::get('/logout', 'Auth\LoginController@logout');
// });
Route::post('/user/create', 'Auth\RegisterController@registerWithApi');

// Route::get('auth/logout', 'Auth\LoginController@logout');


Route::group(['prefix' => 'auth'], function () {
    // Route::post('login', 'AuthController@login');
    // Route::post('signup', 'AuthController@signup');
  
    Route::group(['middleware' => 'auth:api'], function() {
        // Route::get('logout', 'AuthController@logout');
        Route::get('user', 'UserController@isUserAuthenticated');
        Route::get('token_validation', 'AuthController@isTokenValid');

        Route::post('/content_type/create', 'ContentTypeController@create');
        Route::post('/content_type/validate', 'ContentTypeController@CheckIsTitleUsed');
        Route::get('/content_type/list', 'ContentTypeController@list');

        Route::get('/content/field-types', 'FieldTypeController@getFieldTypes');
        Route::get('/content/field-type-fields/{id}', 'ContentTypeFieldController@getFields');

        Route::post('/content-type-field/create', 'ContentTypeFieldController@create');

        Route::get('/content/content-type-fields/{content_type}', 'ContentTypeController@getContentTypeFields');

        Route::post('/content/save', 'ContentController@saveContent');
        Route::get('/content/list', 'ContentController@getContents');
        Route::get('/content/content-values/{id}', 'ContentController@getContentValues');
        Route::post('/content/update/{id}', 'ContentController@updateContent');
    });
});


Route::get('/content/{content_type}/list', 'ContentController@getContentByContentType');


Route::group(['middleware' => 'auth:api'], function() {
    // Route::get('logout', 'AuthController@logout');
    Route::get('users', 'UserController@allUsers');
});







