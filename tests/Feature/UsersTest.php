<?php

namespace Tests\Feature;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;



class UsersTest extends TestCase
{
    use RefreshDatabase;
    /** @test */
    public function user_to_be_added(){
        // $this->session(['email' => 'bar']);
        // $response = $this->post('/api/user/create', $this->postData());

        // // $response->assertSessionHasErrors('email', 'password');
        // $this->assertCount( 0, User::all());

        $this->call('GET', '/');

        $this->assertSessionHas('name');
        $this->assertSessionHas('age', $value);

        $this->assertSessionHasErrors();
    
        // Asserting the session has errors for a given key...
        $this->assertSessionHasErrors('name');
    
        // Asserting the session has errors for several keys...
        $this->assertSessionHasErrors(['name', 'age']);
    }

    public function fields_requierd(){
        collect(['email', 'password'])
            ->each(function($field){
                 $response = $this->post('/api/user/create', array_merge($this->postData(),[ $field => '']));
         
                 $response->assertSessionHasErrors($field);
                 $this->assertCount( 0, User::all());
            });
    }

    private function postData(){
        return [
            '_token' => csrf_token(), 
            'email' => 'admin2@admin.ba',
            'password' => 'DarthVader123#!',
            ];
    }
}
